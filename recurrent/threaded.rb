WAIT_ROLES_UPDATE = 3600

def threaded
  # Code raised every ~SLEEP_TIME seconds
  health_check_server_cache
  refresh_ft_token
  track_nb_users
  tmpban_ops
  verify_users
  roles_updater
end

def refresh_ft_token
  @bot.ft_client = @ft_client = ft_connect(first: false)
end

def track_nb_users
  @bot.game = "#{ENV['FT_DISCORD_PROD'] ? ENV['FT_SERVER_ID'] : @bot.server(YAML.load(
      IO.read(File.join(File.dirname('../'),
                        "config/personal_config.yml")))['server']['ft']).users.count} users on the server !"
end

def tmpban_ops
  collection = @mongo_db[:tmpban_cases]
  all_data = collection.find
  return unless all_data

  all_data.each do |w|
    next unless w[:unban_time] && w[:unban_time] <= Time.now.to_i && @bot.user(w[:user_id])

    @bot.server(@bot.server_id).unban(@bot.user(w[:user_id]), 'Auto unban after ban expiration by 42bots')
    collection.update_one({ user_id: w[:user_id] }, { '$set' => { unban_time: nil } })
  end
end

def verify_users
  return if @bot.verify == true

  @bot.verify = true
  User.where(verified: 0).find_each do |user|
    user.make_all
    user.verified = 1
    user.save
  end
  @bot.verify = false
end

def roles_updater
  collection = @mongo_db[:roles_updater]
  collection.insert_one(type: :time, time: 0) unless collection.find(type: :time).first
  return if @bot.updater == true || collection.find(type: :time).first[:time] + WAIT_ROLES_UPDATE > Time.now.to_i

  @bot.updater = true
  User.where("verified = 1").find_each(&:make_all)
  collection.update_one({ type: :time }, { '$set' => { time: Time.now.to_i } })
  @bot.updater = false
end

puts 'Threaded recurrent Loaded !'.green