@bot.member_join do |event|
  collection = @mongo_db[:add_join_role]
  collection.find({type: 'role_id'}).each { |v| event.user.add_role(v[:role_id]) }
  welcome(event.user)
  join_student(event.user)
  nil
end

def join_student(user)
  User.find_by(discordid: user.id)&.recheck_stud
end

def welcome(user)
  channel_id = ENV['FT_WELCOME_CHANNEL'] ? ENV['FT_WELCOME_CHANNEL'] : YAML.load(IO.read(File.join(File.dirname('../'), "config/personal_config.yml"))).inject({}){|memo,(k,v)| memo[k.to_sym] = v; memo}[:server]['welcome']
  begin
    @bot.send_message(channel_id, "Bienvenue #{user.mention} sur le discord de **42**. Passe du bon temps ici ! Tu peux aller choisir tes rôles dans #{@bot.channel(react_channel_id_get, @bot.server_id).mention}.")
  rescue StandardError => _e
    nil
  end
end

def react_channel_id_get
  ENV['FT_REACT_CHANNEL'] ? ENV['FT_REACT_CHANNEL'] : YAML.load(IO.read(File.join(File.dirname('../'), "config/personal_config.yml"))).inject({}){|memo,(k,v)| memo[k.to_sym] = v; memo}[:server]['react_roles']
end

puts 'Join recurrent Loaded !'.green