REMOTE_CATEG = YAML.load(IO.read(File.join(File.dirname('../'), "config/personal_config.yml"))).inject({}){|memo,(k,v)| memo[k.to_sym] = v; memo}[:server]['remote_categ']
GENERAL_CHAN = YAML.load(IO.read(File.join(File.dirname('../'), "config/personal_config.yml"))).inject({}){|memo,(k,v)| memo[k.to_sym] = v; memo}[:server]['general_chan']
RANGE_VOC = (1..21).to_a
CHANS_VOC = SortedSet.new([])

@bot.voice_state_update do |event|
  chan = event.channel
  old_chan = event.old_channel
  categ = event.server.categories.find { |c| c.id == REMOTE_CATEG }
  general = categ.voice_channels.find { |c| c.id == GENERAL_CHAN }

  if old_chan.nil?
    if chan.parent_id == REMOTE_CATEG && (categ.voice_channels - [general]).select { |g| g.users.empty? }.size.zero?
      num = RANGE_VOC.find { |n| !CHANS_VOC.include? n }
      event.server.create_channel("e3r#{num}", 2, user_limit: 10, parent: REMOTE_CATEG, permission_overwrites: [])
      CHANS_VOC.add(num)
    end
  elsif !old_chan.nil? && !chan.nil?
    if old_chan.parent_id == REMOTE_CATEG && old_chan.id != GENERAL_CHAN && old_chan.users.size.zero? &&
       (categ.voice_channels - [general]).select { |g| g.users.empty? }.size > 1
      old_chan.delete
      num = old_chan.name.split('r')[1].to_i
      CHANS_VOC.delete(num)
    end
    if chan.parent_id == REMOTE_CATEG && chan.id != GENERAL_CHAN && (categ.voice_channels - [general]).select{ |g| g.users.empty? }.size.zero?
      num = RANGE_VOC.find { |n| !CHANS_VOC.include? n }
      event.server.create_channel("e3r#{num}", 2, user_limit: 10, parent: REMOTE_CATEG, permission_overwrites: [])
      CHANS_VOC.add(num)
    end
  elsif old_chan.parent_id == REMOTE_CATEG && old_chan.id != GENERAL_CHAN &&
        old_chan.users.empty? && (categ.voice_channels - [general]).select { |g| g.users.empty? }.size >= 1
    old_chan.delete
    num = old_chan.name.split('r')[1].to_i
    CHANS_VOC.delete(num)
  end
end

puts 'VocCreation recurrent Loaded !'.green
