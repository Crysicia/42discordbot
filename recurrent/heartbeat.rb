# frozen_string_literal: true

@bot.heartbeat do
  health_check_thread
  health_check_server_cache
end

def health_check_thread
  return if @thread&.alive?

  puts 'Thread is dead. Restarting...'.red
  init_thread
end

def health_check_server_cache
  return if @bot.server(@bot.server_id).member_count

  puts 'Error in server cache. Reloading...'.red
  @bot.gateway.inject_reconnect
  @thread&.kill
end

puts 'Heartbeat recurrent Loaded !'.green