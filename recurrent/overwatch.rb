# Overwatch is a COMPLEX message triggered process, so, he belong to the recurrent package


MAX_MESSAGE_STOCK = 10_000
@message_stock = {}
@user_roles_stock = {}

# Message stock : This is the system (at least for now) used to stock temporarly the messages. Discordrb do not provide
# such thing by himself

@bot.message do |event|
  # @type [Discordrb::Events::MessageEvent] event
  @message_stock[event.message.id] = { author: event.author, content: event.content, timestamp: DateTime.now, server: event.server }
  @message_stock.delete(@message_stock.keys.first) if @message_stock.size >= MAX_MESSAGE_STOCK
end

# Message delete : Need to create a system to store every message for a duration, the delete event DO NOT
# provide all the information needed.

@bot.message_delete do |event|
  # @type [Discordrb::Events::MessageDeleteEvent] event
  next unless @message_stock[event.id]

  begin
    channel_id = ENV['FT_LOG_CHANNEL'] ? ENV['FT_LOG_CHANNEL'] : YAML.load(IO.read(File.join(File.dirname('../'), "config/personal_config.yml"))).inject({}){|memo,(k,v)| memo[k.to_sym] = v; memo}[:server]['log']
    event.channel.server.channels.detect { |c| c.id == channel_id }.send_embed do |embed|
      embed.description = "**:wastebasket: Message sent by #{@message_stock[event.id][:author].mention} deleted in #{event.channel.mention}**"
      embed.author = Discordrb::Webhooks::EmbedAuthor.new(name: "#{@message_stock[event.id][:author].name}##{@message_stock[event.id][:author].discriminator}", icon_url: @message_stock[event.id][:author].avatar_url)
      embed.colour = "#000000"
      embed.add_field(value: "#{@message_stock[event.id][:content]}", name: "Deleted message")
      embed.footer = Discordrb::Webhooks::EmbedFooter.new(text: @message_stock[event.id][:server].name)
      embed.timestamp = Time.now
    end
  rescue StandardError => _e
    # ignored
    nil
  end
end

# Message edit : same as before, but only for the content of the message, a lot more information are collected with
# this event


@bot.message_edit do |event|
  # @type [Discordrb::Events::MessageEditEvent] event
  next unless @message_stock[event.message.id]

  channel_id = ENV['FT_LOG_CHANNEL'] ? ENV['FT_LOG_CHANNEL'] : YAML.load(IO.read(File.join(File.dirname('../'), "config/personal_config.yml"))).inject({}){|memo,(k,v)| memo[k.to_sym] = v; memo}[:server]['log']
  event.channel.server.channels.detect { |c| c.id == channel_id }&.send_embed do |embed|
    embed.description = "**:pencil2: Message sent by #{event.author.mention} edited in #{event.channel.mention}**"
    embed.author = Discordrb::Webhooks::EmbedAuthor.new(name: "#{event.author.name}##{event.author.discriminator}", icon_url: event.author.avatar_url)
    embed.colour = "#723cd6"
    embed.add_field(name: 'Old', value: "```#{@message_stock[event.message.id][:content]}```")
    embed.add_field(name: 'New', value: "```#{event.content}```")
    embed.footer = Discordrb::Webhooks::EmbedFooter.new(text: event.server.name)
    embed.timestamp = Time.now
  end
end

# Member join : Used when a member joined the server. The display is actually pretty simple and can be modify later on

@bot.member_join do |event|
  channel_id = ENV['FT_LOG_CHANNEL'] ? ENV['FT_LOG_CHANNEL'] : YAML.load(IO.read(File.join(File.dirname('../'), "config/personal_config.yml"))).inject({}){|memo,(k,v)| memo[k.to_sym] = v; memo}[:server]['log']
  event.server.channels.detect { |c| c.id == channel_id }.send_embed do |embed|
    embed.description = "**#{event.user.mention} joined the server**"
    embed.author = Discordrb::Webhooks::EmbedAuthor.new(name: "#{event.user.name}##{event.user.discriminator}", icon_url: event.user.avatar_url)
    embed.colour = "#0ce305"
    embed.add_field(name: ':timer: Age of account', value: "`#{event.user.creation_time.strftime("%Y/%m/%d %T")}`\n**#{(Time.now.year * 12 + Time.now.month) - (event.user.creation_time.year * 12 + event.user.creation_time.month)} months ago**\n**#{(Time.now - event.user.creation_time).round} seconds ago**")
    embed.footer = Discordrb::Webhooks::EmbedFooter.new(text: event.server.name)
    embed.timestamp = Time.now
    embed.thumbnail = Discordrb::Webhooks::EmbedThumbnail.new(url: event.user.avatar_url)
  end
end

# Member leave : Used when a member leave the server. Do not use mention in it, discord had some difficulty to handle the fact
# that a member no longer reachable can be mentioned in an embed

@bot.member_leave do |event|
  channel_id = ENV['FT_LOG_CHANNEL'] ? ENV['FT_LOG_CHANNEL'] : YAML.load(IO.read(File.join(File.dirname('../'), "config/personal_config.yml"))).inject({}){|memo,(k,v)| memo[k.to_sym] = v; memo}[:server]['log']
  event.server.channels.detect { |c| c.id == channel_id }.send_embed do |embed|
    embed.description = "**#{event.user.name} left**"
    embed.author = Discordrb::Webhooks::EmbedAuthor.new(name: "#{event.user.name}##{event.user.discriminator}", icon_url: event.user.avatar_url)
    embed.colour = "#e02238"
    embed.footer = Discordrb::Webhooks::EmbedFooter.new(text: event.server.name)
    embed.timestamp = Time.now
    embed.thumbnail = Discordrb::Webhooks::EmbedThumbnail.new(url: event.user.avatar_url)
  end
end

# Member update : Discordrb only provide the information that an user as been updated. It could be different things,
# such as a Role Added/Deleted from the user, a NickName change or some strange stuff. However,
# a caching system is needed to use this method

@bot.member_update do |event|
  begin
    channel_id = ENV['FT_LOG_CHANNEL'] ? ENV['FT_LOG_CHANNEL'] : YAML.load(IO.read(File.join(File.dirname('../'), "config/personal_config.yml"))).inject({}){|memo,(k,v)| memo[k.to_sym] = v; memo}[:server]['log']
    next if @user_roles_stock.empty?

    # Check that the member still has each role we knew they had,
    # and handle if it was removed
    new_roles_ids = event.roles.map(&:id)
    remaining_roles = []
    deleted_roles = ''
    added_roles = ''
    @user_roles_stock[event.user.id]&.each do |id|
      if new_roles_ids.include?(id)
        remaining_roles << id
      elsif event.server.role(id)
        deleted_roles << ":no_entry: #{event.server.role(id).name}\n"
      end
    end

    # Check for added roles
    added_role_ids = new_roles_ids - remaining_roles
    added_role_ids.each do |id|
      added_roles << ":white_check_mark: #{event.server.role(id).name}\n"
    end

    caching_user_roles(event.user)
    next if added_roles.empty? && deleted_roles.empty?

    event.server.channels.detect { |c| c.id == channel_id }.send_embed do |embed|
      embed.description = ":writing_hand:** #{event.user.mention} has been updated**"
      embed.author = Discordrb::Webhooks::EmbedAuthor.new(name: "#{event.user.name}##{event.user.discriminator}", icon_url: event.user.avatar_url)
      embed.colour = "#e02238"
      embed.add_field(name: 'Roles', value: deleted_roles + added_roles)
      embed.footer = Discordrb::Webhooks::EmbedFooter.new(text: event.server.name)
      embed.timestamp = Time.now
      embed.thumbnail = Discordrb::Webhooks::EmbedThumbnail.new(url: event.user.avatar_url)
    end
  rescue StandardError => e
    nil
  end
end

# Channel Create : Raise when a channel is created. Provide a lot of useful information. Guard close is used to prevent
# pre-loading ghost event

@bot.channel_create do |event|
  next unless event.server

  channel_id = ENV['FT_LOG_CHANNEL'] ? ENV['FT_LOG_CHANNEL'] : YAML.load(IO.read(File.join(File.dirname('../'), "config/personal_config.yml"))).inject({}){|memo,(k,v)| memo[k.to_sym] = v; memo}[:server]['log']
  event.server.channels.detect { |c| c.id == channel_id }.send_embed do |embed|
    embed.description = ":restroom: **Channel Created: `#{event.channel.name}`**"
    embed.author = Discordrb::Webhooks::EmbedAuthor.new(name: event.server.name, icon_url: event.server.icon_url)
    embed.colour = "#fcba03"
    embed.footer = Discordrb::Webhooks::EmbedFooter.new(text: event.server.name)
    embed.timestamp = Time.now
  end
end

# Channel Delete : Same as channel create, but the 'channel' object is not reachable (it does not exist in fact). However,
# Id, name, type, position and topic can be found directly in the 'event' object

@bot.channel_delete do |event|
  channel_id = ENV['FT_LOG_CHANNEL'] ? ENV['FT_LOG_CHANNEL'] : YAML.load(IO.read(File.join(File.dirname('../'), "config/personal_config.yml"))).inject({}){|memo,(k,v)| memo[k.to_sym] = v; memo}[:server]['log']
  event.server.channels.detect { |c| c.id == channel_id }.send_embed do |embed|
    embed.description = ":restroom: **Channel Deleted: `#{event.name}`**"
    embed.author = Discordrb::Webhooks::EmbedAuthor.new(name: event.name, icon_url: event.server.icon_url)
    embed.colour = "#eb4034"
    embed.footer = Discordrb::Webhooks::EmbedFooter.new(text: event.server.name)
    embed.timestamp = Time.now
  end
end

# Voice State Update : Used to track the "voice" activity of every users on the server. It provide a lot of usefulls
# information.

@bot.voice_state_update do |event|
  channel_id = ENV['FT_LOG_CHANNEL'] ? ENV['FT_LOG_CHANNEL'] : YAML.load(IO.read(File.join(File.dirname('../'), "config/personal_config.yml"))).inject({}){|memo,(k,v)| memo[k.to_sym] = v; memo}[:server]['log']
  if event.old_channel && event.old_channel != event.channel
    event.server.channels.detect { |c| c.id == channel_id }.send_embed do |embed|
      embed.description = ":microphone:** #{event.user.mention} left voice channel `#{event.old_channel.name}`**"
      embed.author = Discordrb::Webhooks::EmbedAuthor.new(name: "#{event.user.name}##{event.user.discriminator}", icon_url: event.user.avatar_url)
      embed.colour = "#1e6475"
      embed.footer = Discordrb::Webhooks::EmbedFooter.new(text: event.server.name)
      embed.timestamp = Time.now
    end
  end

  if event.channel && event.channel != event.old_channel
    event.server.channels.detect { |c| c.id == channel_id }.send_embed do |embed|
      embed.description = ":microphone:** #{event.user.mention} joined voice channel `#{event.channel.name}`**"
      embed.author = Discordrb::Webhooks::EmbedAuthor.new(name: "#{event.user.name}##{event.user.discriminator}", icon_url: event.user.avatar_url)
      embed.colour = "#42d1f5"
      embed.footer = Discordrb::Webhooks::EmbedFooter.new(text: event.server.name)
      embed.timestamp = Time.now
    end
  end
end

puts 'Overwatch recurrent Loaded !'.green