# frozen_string_literal: true

module Discordrb
  # Change the reconnect factor on error 4003
  class Gateway
    private

    def handle_close(e)
      @bot.__send__(:raise_event, Discordrb::Events::DisconnectEvent.new(@bot))

      if e.respond_to? :code
        # It is a proper close frame we're dealing with, print reason and message to console
        LOGGER.error('Websocket close frame received!')
        LOGGER.error("Code: #{e.code}")
        LOGGER.error("Message: #{e.data}")
        @should_reconnect = false if [4004, 4011].include?(e.code)
      elsif e.is_a? Exception
        # Log the exception
        LOGGER.error('The websocket connection has closed due to an error!')
        LOGGER.log_exception(e)
      else
        LOGGER.error("The websocket connection has closed: #{e&.inspect || '(no information)'}")
      end
    end
  end
end

# Simple ErrorClass for testing purposes
class TweakError < StandardError
  attr_reader :code, :data
  def initialize(msg = 'Default message', data = 'nothing', code: 401)
    @code = code
    @data = data
    super(msg)
  end
end