# Warn: Add a warn to an user. It' stored in the mongo_db database.

@bot.command(:warn, min_args: 1, required_permissions: [:kick_members]) do |event, user, *reason|
  collection = @mongo_db[:judiciary_cases]
  collection.insert_one(user_id: @bot.parse_mention(user).id, warns: []) unless collection.find(user_id: @bot.parse_mention(user).id).first

  collection.update_one({ user_id: @bot.parse_mention(user).id},
                         '$set' => { warns: collection.find(user_id: @bot.parse_mention(user).id).first[:warns] \
                        << {timestamp: Time.now.to_i, reason: reason.join(' '),
                            warn_id: SecureRandom.uuid} } )
  begin
    @bot.parse_mention(user).pm.send_embed do |embed|
      embed.description = ':warning: **You were warned !**'
      embed.add_field(name: 'Warning message:',
                      value: reason.join(' '))
      embed.footer = Discordrb::Webhooks::EmbedFooter.new(text: event.server.name, icon_url: event.server.icon_url)
      embed.timestamp = Time.now
      embed.colour = '#fcba03'
    end
  rescue StandardError => e
    puts "Error while sending warning message to #{user} : #{e.message}"
  end
  event.message.react('✅')
end

# List_warn: See the warns against an user.

@bot.command(:list_warn, aliases: [:warn_list], min_args: 1, max_args: 1, required_permissions: [:kick_members]) do |event, user|
  collection = @mongo_db[:judiciary_cases]
  next unless (user_obj = @bot.parse_mention(user))

  collection.insert_one(user_id: user_obj.id, warns: []) unless collection.find(user_id: user_obj.id).first
  next unless (user_warns = collection.find(user_id: user_obj.id)).first

  event.channel.send_embed do |embed|
    embed.description = ":octagonal_sign: **#{user_obj.name}\##{user_obj.discriminator}: warn's list**"
    embed.author = Discordrb::Webhooks::EmbedAuthor.new(name: user_obj.name, icon_url: event.server.icon_url)
    embed.colour = '#9e1f16'
    user_warns.first[:warns].each do |w|
      embed.add_field(name: ":stopwatch: #{Time.at(w['timestamp'])}",
                      value: "Warn ID (**#{w['warn_id']}**)\nUser: #{user_obj.mention}\n```\n#{w['reason']}\n```")
    end
    embed.footer = Discordrb::Webhooks::EmbedFooter.new(text: event.server.name)
    embed.timestamp = Time.now
  end
end

@bot.command(:delete_warn, min_args: 1, required_permissions: [:kick_members]) do |event, *w_id|
  collection = @mongo_db[:judiciary_cases]
  w_id.each do |id|
    next event.respond "**No warn with the id `#{id}` have been found.**" unless (deleted_warn = collection.find('warns.warn_id'=> id)).first

    collection.update_one({ user_id: deleted_warn.first[:user_id]},
                          '$set' => { warns: deleted_warn.first[:warns].reject { |w| w[:warn_id] == id } })
    event.respond "**Warn with id `#{id}` have been deleted.**"
  end
  event.message.react('✅')
end

@bot.command(:kick, min_args: 1, required_permissions: [:kick_members]) do |event, user, *reason|
  reason = reason.join(' ') if reason
  reason << " by #{event.user.display_name}"
  @bot.server(@bot.server_id).kick(@bot.parse_mention(user), reason)
  event.message.react('✅')
end

@bot.command(:ban, min_args: 1, required_permissions: [:ban_members]) do |event, user, *reason|
  if reason && !reason.first.to_i.zero?
    day_delete = reason.first.to_i > 7 ? 7 : reason.first.to_i
    reason.shift
  end
  day_delete ||= 0
  reason = reason.join(' ') if reason
  reason << " by #{event.user.display_name}"
  @bot.server(@bot.server_id).ban(@bot.parse_mention(user), day_delete, reason: reason)
  event.message.react('✅')
end

@bot.command(:unban, min_args: 1, required_permissions: [:ban_members]) do |event, user, *reason|
  obj_user = @bot.parse_mention(user) if @bot.parse_mention(user)
  obj_user ||= @bot.user(user)
  reason = reason.join(' ') if reason
  reason << " by #{event.user.display_name}"
  @bot.server(@bot.server_id).unban(obj_user, reason)
  event.message.react('✅')
end

# A huge part of the process occur in /recurrent/heartbeat.rb
@bot.command(:tmpban, min_args: 2, required_permissions: [:ban_members]) do |event, user, time, *reason|
  collection = @mongo_db[:tmpban_cases]
  collection.insert_one(user_id: @bot.parse_mention(user).id, unban_time: nil) unless collection.find(user_id: @bot.parse_mention(user).id ).first

  collection.update_one({ user_id: @bot.parse_mention(user).id }, '$set' => { unban_time: Time.now.to_i + time.to_i * 60 })
  if reason && !reason.first.to_i.zero?
    day_delete = reason.first.to_i > 7 ? 7 : reason.first.to_i
    reason.shift
  end
  day_delete ||= 0
  reason = reason.join(' ') if reason
  reason << " by #{event.user.display_name}"
  @bot.server(@bot.server_id).ban(@bot.parse_mention(user), day_delete, reason: reason)

  event.message.react('✅')
end

CLEAR_DESC = "Permet de supprimer un grand nombre de messages (entre 1 et 99). Si un utilisateur est ciblé, \
seul ces messages sont supprimés dans le nombre de messages visées.".freeze

# Used to delete a large number of messages
@bot.command(:clear, min_args: 1, max_args: 2, required_permissions: [:kick_members], usage: 'clear <nbr> <user>', description: CLEAR_DESC, aliases: [:clean, :purge, :delete]) do |event, nbr, user|
  nbr = nbr.to_i
  return event.respond 'Erreur, le nombre maximum de messages est 99 et doit etre supérieur a 0.' unless nbr <= 99 && nbr > 0

  user = @bot.parse_mention(user) if user
  event.message.delete
  prune = user ? event.channel.prune(nbr) { |m| m.author.id == user.id } : event.channel.prune(nbr)
  event.channel.send_temporary_message("```c\n#{prune} message#{'s' if prune > 1} #{"by #{user.name}" if user}\
#{prune <= 1 ? 'has' : 'have'} been deleted.\n```", 3)
end

puts 'Moderation CMD Loaded !'.green
