# frozen_string_literal: true

NUM_EMOJIS = {
  0 => "0⃣",
  1 => "1⃣",
  2 => "2⃣",
  3 => "3⃣",
  4 => "4⃣",
  5 => "5⃣",
  6 => "6⃣",
  7 => "7⃣",
  8 => "8⃣",
  9 => "9⃣",
}.freeze


@bot.command(:poll, description: 'Outil de sondage (1⃣,2⃣,3⃣,...,9⃣)', usage: '!poll Question | réponse 1 | réponse 2 | autres réponses... (max 10)', min_args: 3) do |event, *args|
  event.message.delete

  args = args.join(' ').split('|')
  question = args[0]
  responses = args[1..args.size-1]

  if responses.size > 10
    event.respond("Merci de ne pas entrer plus de 10 réponses possibles. (Ouais j'avais la flemme)")
    return
  end

  message = event.channel.send_embed do |embed|
    embed.color = 5800090

    desc = "🗳️ **#{question}**\n\n"
    responses.each_with_index do |response, index|
      index = index + 1 unless responses.size > 9
      desc = desc + "#{NUM_EMOJIS[index]} #{response}\n"
    end

    embed.description = desc
  end

  responses.each_with_index do |r, index|
    index = index + 1 unless responses.size > 9
    message.react(NUM_EMOJIS[index])
  end

  return
end


@bot.command(:pollyesno, aliases: [:pollyn], description: 'Outil de sondage Yes/No (👍/👎)', usage: '!pollyn Question', min_args: 1) do |event, *args|
  event.message.delete

  question = args.join(' ')

  message = event.channel.send_embed do |embed|
    embed.color = 5800090
    embed.description = "🗳️ **#{question}**"
  end

  message.react("👍")
  message.react("👎")

  return
end


puts 'Poll CMD Loaded !'.green