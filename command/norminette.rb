NORM_HELP = "Please wrap your code between a `C` markdown :\n \\```c\n_Your code_\n```\n\nNotes: Discord do not handle tabs, so to emulate it you need te represent a tab with '2' spaces.\nUsing the tab key in a 'code block' from Discord will simply puts '2' spaces.\nYou can also use the '-S' option to change this value.\n\nAlso, this norminette will never watch for the file's name or the 42 header.\nOn another topic, blank lines at the top and the bottom of the 'code block' does not print, but they are still sent to Discord."

@bot.command(:norminette, usage: 'norminette [code]', description: NORM_HELP) do |event, help|
  return event.respond NORM_HELP if !help || help == 'help'

  return event.channel.split_send norminette(['-h'], no_code: true) if %w[-h --help].include?(help)

  return event.channel.split_send norminette(['-v'], no_code: true) if %w[-v --version].include?(help)

  args = event.message.content.sub(/^#{@bot.prefix}#{event.command.name} /, '')
  code = /(?<=```c\s).*?(?=```)/ims.match(args).to_s
  args = args.gsub(/```c\s.*?```/ims, '').split(' ')

  return event.respond "No code detected. #{NORM_HELP}" if code.empty?

  code = tweak_code(code, option_parser(args)[:space_tab])
  code_file = Tempfile.new(%w[norminette .c])
  code_file.write("\n#{code}")
  code_file.rewind
  event.channel.split_send norminette(%w[-R CheckFilename -R CheckTopCommentHeader] + args << code_file.path)
  code_file.close
  code_file.unlink
  nil
end

def tweak_code(code, space = 2)
  space_to_tab(code, space)
end

def space_to_tab(code, space)
  code.gsub(/ {#{space}}/, "\t")
end

def option_parser(args)
  Slop.parse(args, suppress_errors: true) do |o|
    o.int '-S', '--space_tab', 'How many space for a tab', default: 4
  end
end

puts 'Norminette CMD Loaded !'.green