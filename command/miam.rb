LA_BOUF = [{ '🍝' => 'Pates' },
           { '🥓' => 'SteakOverflow' },
           { '🥑' => 'Nestor' },
           { '🥙' => 'Kebab' },
           { '🌮' => 'Tacos' },
           { '🍟' => 'Burger King' },
           { '🍔' => 'BAM' },
           { '🌽' => 'Vegan Hero' },
           { '🥪' => 'La petite cabane' },
           { '🍕' => 'Pizza' },
           { '🌯' => 'Kumo' },
           { '💰' => 'Uber Eats / Deliveroo' },
           { '🥖' => 'Pain' },
           { '🍙' => 'Chinois' },
           { '🍣' => 'Japonais' }].freeze


@bot.command(:gfaim, description: 'Found where you want eat', usage: 'gfaim', aliases: [:miam, :bouffe]) do |event|
  r = Random.new

  rand = r.rand(0..LA_BOUF.size - 1)
  the_bouf = LA_BOUF[rand].shift

  event.channel.send_embed do |embed|
    embed.color = 5800090
    embed.description = "#{the_bouf[0]}   **#{the_bouf[1]}**"
  end

end
