LANGS = [
  { file: 'fr', flag: 'fr', name: 'Français' },
  { file: 'en', flag: 'us', name: 'English' },
]

@bot.command(:norm, aliases: [:norme], description: 'Envoie le PDF de la norme (FR & EN)', usage: '!norm [fr/en]') do |event, arg|
  LANGS.each do |lang|
    event.channel.send_file(
      File.new("assets/norme.#{lang[:file]}.pdf", "r"),
      caption: ":flag_#{lang[:flag]}: #{lang[:name]} :arrow_down:"
    ) if (arg.nil? || arg == lang[:file] || !(langs.map{|l| l[:file]}.include? arg))
  end
  nil
end

puts 'Norm CMD Loaded !'.green
