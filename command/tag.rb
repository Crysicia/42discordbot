@bot.command(:tag, min_args: 1) do |event, name|
  collection = @mongo_db[:tags]
  return event.respond 'No tag with this name has been saved.' unless (tag = collection.find(name: name.downcase).first)

  event.respond tag[:content]
end

@bot.command(:settag, min_args: 2, required_permissions: [:kick_members]) do |event, name|
  collection = @mongo_db[:tags]
  collection.insert_one(name: name.downcase, content: nil) unless collection.find(name: name.downcase).first

  tag_message = event.message.content.sub(/^#{@bot.prefix}#{event.command.name} #{name}/, '')
  collection.update_one({ name: name.downcase }, '$set' => { content: tag_message })

  event.message.react('✅')
end

puts 'Tag CMD Loaded !'.green