class ApplicationRecord < ActiveRecord::Base
  self.abstract_class = true
end

hash_arg = YAML.load(IO.read(File.join(File.dirname('../'), "config/personal_config.yml"))).inject({}){|memo,(k,v)| memo[k.to_sym] = v; memo}
ActiveRecord::Base.establish_connection(
    adapter: 'mysql2',
    host: hash_arg[:SQL]['adresse'],
    username: hash_arg[:SQL]['account'],
    password: hash_arg[:SQL]['password'],
    database: hash_arg[:SQL]['db']
)

puts "ActionRecord Initialized !".green

