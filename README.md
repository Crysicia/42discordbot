# DiscordBot

## Général 

* Version Bundler : 2.0.2 (min >= 2.0.2)

* Version Ruby : 2.5.1 (min >= 2.5.0)

* Version Git : 2.11.0 (min >= 2.5.0)

## Windows

* Downloaded latest pre-built binary e.g. libsodium-1.0.18-msvc.zip : https://download.libsodium.org/libsodium/releases/

* Copy `libsodium-1.0.18-msvc\libsodium\x64\Release\v142\dynamic\libsodium.dll` to `C:\Windows\System32\sodium.dll` (rename it)

* Open a Windows CMD (or PowerShell) and `ridk install` for update if ffi is still complaining about something

* MySQL Connector should be necessary

* Git (not an application like `git kraken` or `git bash`, the true one) is mandatory to pull some of the gems. 
On Windows, you can install it with the `choco` command line : `choco install git.install`  or directly on
https://gitforwindows.org/

## OSX

* Also, you can add in your env `RBNACL_LIBSODIUM_GEM_LIB_PATH` and directly target the `libsodium.dylib` file (you can find 
it in the `~/.brew/lib` after using `brew install libsodium`), it's usefull on macos 
if you install libsodium with brew

* Make sure that Git is already installed with `brew install git`

## Organisation

* `/class` For every new (general) object

* `/helper` For every helper module

* `/config` Configs files for the bot

* `/command` Every command of the bot and standard messages triggered process in a standard ruby file

* `/recurrent` Where all the recurrent process belong
##

DiscordBot for 42 server