$current_path = Dir.pwd

# if File.symlink?(__FILE__)
#   dir = File.expand_path(File.dirname(File.readlink(__FILE__)))
#   Dir.chdir dir
# else
#   dir = File.expand_path(File.dirname(__FILE__))
#   Dir.chdir dir
# end

$config = ParseConfig.new('config/norminette_config.conf')

Bundler.require

class Norminotron

  attr_accessor :reply, :norminette

  def initialize
    @reply = ''
    @norminette = Norminette.new
  end

  class Sender
    def initialize &block
      @conn = Bunny.new       hostname:       $config['hostname'],
                              vhost:          '/',
                              user:           $config['user'],
                              password:       $config['password']

      @conn.start
      @ch                     = @conn.create_channel
      @x                      = @ch.default_exchange
      @reply_queue    = @ch.queue('', exclusive: true)
      @lock                   = Mutex.new
      @condition              = ConditionVariable.new
      @routing_key    = 'norminette'
      @counter                = 0

      @reply_queue.subscribe do |delivery_info, properties, payload|
        @counter -= 1
        block.call delivery_info, properties, payload
        @lock.synchronize { @condition.signal }
      end

      at_exit { desinitialize }
    end

    def desinitialize
      @ch.close if @ch
      @conn.close if @conn
    end

    def publish content
      @counter += 1
      @x.publish content,     routing_key:  @routing_key,
                 reply_to:     @reply_queue.name,
                 correlation_id: SecureRandom.uuid
    end

    def sync_if_needed
      @lock.synchronize { @condition.wait(@lock) }
    end

    def sync
      sync_if_needed until @counter == 0
    end
  end



  class Norminette
    attr_accessor :reply

    def initialize()
      @reply = ''
      @files                  = []
      @sender                 = Sender.new do |delivery_info, properties, payload|
        manage_result(JSON.parse(payload))
      end
    end

    def check files_or_directories, options
      return if options == :exit

      if options.version
        version
      else
        populate_recursive(files_or_directories.any? ? files_or_directories : [$current_path])
        send_files options
      end

      @sender.sync
    end

    private

    def populate_recursive(objects)
      objects.each do |object|
        object = (Pathname.new(object).absolute? ? object : File.join($current_path, object))

        if File.directory? object
          populate_recursive(Dir["#{object}/*"])
        else
          populate_file(object)
        end
      end
    end

    def version
      # puts "Local version:\n1.0.0.rc1"
      # puts "Norminette version:"
      send_content({action: 'version'}.to_json)
    end

    def file_description file, opts = {}
      ({filename: file, content: File.read(file)}.merge(opts)).to_json
    end

    def is_a_valid_file? file
      File.file? file and File.exists? file and file =~ /\.[ch]\z/
    end

    def populate_file(file)
      unless is_a_valid_file? file
        # manage_result({ 'filename' => file, 'display' => "Warning: Not a valid file\n" })
        return
      end

      @files << file
    end

    def send_files options
      @files.each do |file|
        send_file file, options.rules
        @sender.sync_if_needed
      end
    end

    def send_file file, rules
      send_content file_description(file, rules: rules)
    end

    def send_content content
      @sender.publish content
    end

    def cleanify_path filename
      File.expand_path(filename).gsub(/^#$current_path\/?/, './')
    end

    def manage_result(result)
      @reply << "Norme: CODE_BLOCK\n"      if result['filename']
      @reply << result['display']                                                          if result['display']
      # exit 0                                                                                          if result['stop'] == true
    end
  end

  class Parser
    def self.parse(options, reply)
      args = OpenStruct.new
      opt_parser = OptionParser.new do |opts|
        opts.banner = 'Usage: norminette [option] [code]'

        opts.on('-v', '--version', 'Print version') do |_n|
          args.version = true
        end

        opts.on('-R', '--rules Array', Array, 'Rule to disable') do |rules|
          args.rules ||= []
          args.rules += rules
        end

        opts.on('-S', '--space_tab', 'How many space for a tab') do |_|
          # only here to not display bad option message
        end

        opts.on('-h', '--help', 'Prints this help') do
          sender  = Sender.new do |_delivery_info, _properties, payload|
            reply <<  JSON.parse(payload)['display']
          end

          reply << opts.to_s
          reply << 'Norminette usage:'
          sender.publish({action: 'help'}.to_json)
          sender.sync
          return :exit
        end
      end

      begin
        opt_parser.parse!(options)
      rescue StandardError => e
        reply << "#{e.message}\n"
      end

      args
    end
  end
end

def patch_name_file(reply, file_name)
  # Used to obfuscate the file name
  reply.gsub(/#{Regexp.escape(file_name)}/m, 'code')
end

def patch_lines(reply)
  # Because we manually add a `\n` at the start of the file, we need to tweak the output's lines numbers
  reply.gsub(/(?<=Error \(line )\d*/m) { |line| (line.to_i - 1).to_s }
end

def patch_reply(reply, file_name)
  patch_name_file(patch_lines(reply), file_name)
end

def norminette(argv = [], no_code: false)
  norm = Norminotron.new.norminette
  norm.check(argv, Norminotron::Parser.parse(argv, norm.reply))
  no_code ? norm.reply : patch_reply(norm.reply, argv.last)
rescue StandardError => e
  # Need this block to prevent the whole bot to crash if something wrong happen here
  puts "Error in norminette : #{e.message}"
end