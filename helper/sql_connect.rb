# frozen_string_literal: true

def sql_connect
  begin
    if ENV['FT_DISCORD_PROD']
      dbh = ActiveRecord::Base.establish_connection(
          adapter: 'mysql2',
          host: ENV['FT_SQL_ADRESSE'],
          username: ENV['FT_SQL_USER'],
          password: ENV['FT_SQL_PASSWORD'],
          database: ENV['FT_SQL_DV']
      )
    else
      hash_arg = YAML.load(IO.read(File.join(File.dirname('../'), "config/personal_config.yml"))).inject({}){|memo,(k,v)| memo[k.to_sym] = v; memo}
      dbh =ActiveRecord::Base.establish_connection(
          adapter: 'mysql2',
          host: hash_arg[:SQL]['adresse'],
          username: hash_arg[:SQL]['user'],
          password: hash_arg[:SQL]['password'],
          database: hash_arg[:SQL]['db']
      )
    end
    puts "SQL Initialized !".green
    dbh
  rescue Mysql2::Error => e
    puts "Code d'erreur : #{e.errno}"
    puts "Message d'erreur : #{e.error}"
    puts "SQLSTATE d'erreur : #{e.sqlstate}" if e.respond_to?("sqlstate")
    puts "SQL failed to Initialize !".red
  end
end