def get_json(url, token)
  uri = URI.parse(url)
  request = Net::HTTP::Get.new(uri)
  request["Authorization"] = token
  req_options = {
      use_ssl: uri.scheme == "https",
  }
  response = Net::HTTP.start(uri.hostname, uri.port, req_options) do |http|
    http.request(request)
  end
  JSON.parse(response.body)
end
