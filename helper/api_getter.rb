# frozen_string_literal: true

# Simple 42 API getter
# @param request_string [String] the API endpoint path
# @param filter [Hash] the filters, encoded like : {pool_year: 2020, pool_month: 'july', primary_campus_id: 1}
# @param per_page [Integer] numbers of elements per_page, max 100 and by default 100
# @param page [Integer] the page wanted, act as a starting page with the 'all' option
# @param all [Boolean] set to true if you want to get all matching elements, act as a recursive call
# @param other_params [Hash] specific parameters, act like filters but have a different name, encoded like :
#                            {project_id: 1258, partnership_id: 14}
# @param block [Proc] a block, converted as a Proc bind to the current scope, raised after the result is processed
# @return [Hash, Symbol] the requested data chunk
def api_getter(request_string, filter: nil, per_page: 100, page: 1, all: false, other_params: nil, &block)
  retries ||= 0
  encoded_params = encode_params(filter, per_page, page, other_params)
  result = @ft_client.get(request_string, params: encoded_params).parsed
  if result.count.positive?
    block&.call_with_binding(binding)
    if all
      result += api_getter(request_string, filter: filter, per_page: per_page, page: page + 1, all: true,
                                           other_params: other_params, &block)
    end
  end
  result
rescue OAuth2::Error => e
  return :not_reachable if e.message == ": \n{}"

  sleep(TOKEN_REQUEST_SECOND)
  retry if (retries += 1) < 5
end

def encode_params(filter, per_page, page, other_params)
  params = { filter: {}, per_page: per_page, page: page }
  filter&.each { |k, v| params[:filter][k] = v }
  other_params&.each { |k, v| params[k] = v }
  params
end