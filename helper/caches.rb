def init_users_roles_cache
  server = ENV['FT_DISCORD_PROD'] ? ENV['FT_SERVER_ID'] : @bot.server(YAML.load(
      IO.read(File.join(File.dirname('../'), "config/personal_config.yml")))['server']['ft'])
  server.members.each { |member| caching_user_roles(member) }
end

def caching_user_roles(member)
  @user_roles_stock[member.id] = member.roles.map { |role| role.id }
end