# frozen_string_literal: true

def ft_linked?(event)
  return false unless User.find_by(discordid: event.user.id)&.verified

  hash_arg = YAML.load(IO.read(File.join(File.dirname('../'), 'config/personal_config.yml'))).inject({}){|memo,(k,v)| memo[k.to_sym] = v; memo}[:server]
  return event.channel if event.channel.parent_id == hash_arg['student_categ'] || event.channel.parent_id == hash_arg['admin_categ']

  event.respond 'Envoie des informations par message privé.'
  event.user.pm
end
