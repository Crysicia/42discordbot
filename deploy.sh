#!/bin/sh

export HOME=/home/ft
if [ -d $HOME/.rbenv ]; then
  export PATH="$HOME/.rbenv/shims:$PATH"
  eval "$(rbenv init -)"
fi
rbenv local 2.5.1
git pull
bundle install
while true; do
  ruby main.rb
  sleep 1
done