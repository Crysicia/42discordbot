RBNACL_LIBSODIUM_GEM_LIB_PATH = ENV['RBNACL_LIBSODIUM_GEM_LIB_PATH'] if ENV['RBNACL_LIBSODIUM_GEM_LIB_PATH']
SLEEP_TIME = 20

require 'date'
require 'securerandom'
require 'rubygems'
require 'bundler/setup'
require 'pry'
require 'oauth2'
require 'mysql2'
require 'net/http'
require 'uri'
require 'json'
require 'mongo'
require 'fileutils'
require 'discordrb'
require 'colorize'
require 'active_record'
require 'awesome_print'
require 'irb'
require 'down'
require 'imgur'
require 'optparse'
require 'parseconfig'
require 'securerandom'
require 'slop'
require 'net/http'
require 'uri'
require 'dotiw'
require 'date'

load 'class/ft_bot.rb'
load 'class/string.rb'
load 'class/imgur_patch.rb'
load 'class/drb_patcher.rb'
load 'class/proc_tweaker.rb'

load 'helper/sql_connect.rb'
load 'helper/mongo_connect.rb'
load 'helper/ft_connect.rb'
load 'helper/navitia_token.rb'
load 'helper/json_getter.rb'
load 'helper/caches.rb'
load 'helper/reaction.rb'
load 'helper/link_verification.rb'
load 'helper/imgur_connect.rb'
load 'helper/norminette_patched.rb'
load 'helper/api_getter.rb'

def init_thread
  unless @thread&.alive?
    @thread = Thread.new do
      puts 'Thread Initialized !'.green
      loop do
        Thread.new { threaded }
        sleep(SLEEP_TIME)
      end
    end
  end
end

# Process start after the bot is fully prepared
def finish_init
  @bot.ready do
    unless @bot.server(@bot.server_id).member_count
      puts 'Error in server cache. Reloading...'.red
      next @bot.gateway.inject_reconnect
    end
    init_users_roles_cache
    @thread&.kill
    init_thread
    puts 'Bot fully initialized!'.blue
  end
  at_exit { @bot.stop }
end

# The main loop of the project, everything start from here and happen here.
def main
  puts 'Hello World !'.green

  @bot = FTBot.instance
  puts 'Start connections...'.blue
  @sql_db = sql_connect
  @mongo_db = mongo_connect
  @imgur = imgur_connect
  @bot.ft_client = @ft_client = ft_connect
  @navitia = navitia_token

  puts 'Start loading ActionRecord...'.blue
  load './model/application.rb'
  puts 'Start loading models...'.blue
  Dir['./model/*'].each { |f| load f unless f == './model/application.rb' }

  puts 'Start loading commands...'.blue
  Dir['./command/*'].each { |f| load f }

  puts 'Start loading recurents...'.blue
  Dir['./recurrent/*'].each { |f| load f }

  finish_init

  @bot.run(:async)

  loop do
    arg = gets.chomp

    exit if arg == 'stop'
  end
end

main